var bossSprite;
var bossLevel;
var health;
var damage;
var defence;
var loot;

function init(level, sprite) {
	bossSprite = sprite;
	bossLevel = level;
	health = bossLevel * 50;
	damage = bossLevel;
	defence = bossLevel;
}