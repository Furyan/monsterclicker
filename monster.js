function Monster(sprite, level, health, damage, defense, loot) {
	this.sprite = sprite;
	this.level = level;
	this.health = health;
	this.damage = damage;
	this.defense = defense;
	this.loot = loot;

	function getHealth() {
		return this.health;
	}

	function getReward() {
		return this.loot;
	}

	function setHealth(health) {
		this.health = health;
	}

}
